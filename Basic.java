import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;
import org.w3c.dom.ls.LSOutput;

import java.util.Scanner;

public class Basic {
    public static void main(String[] args) {
        //getHello();
        //getValue();
        //getString();
        //getDivision();
        //getMax();
        //threeValues();
        compare();

    }

    public static void getHello() {
        System.out.print("Hello World\nHello World\n");
    }

    public static void getValue() {
        double value = 7.55555;
        System.out.printf("value: %.2f\n ", value);
    }

    public static void getString() {
    String studentName = "student name";
    String heading1 = "Exam_Name";
    String heading2 = "Exam_Grade";
        System.out.printf("%-15s %-15s %15s", studentName, heading1, heading2);
        System.out.println();
    }

    public static void getDivision(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Type first number ");
        int a = scanner.nextInt();
        System.out.println("Type second number ");
        int b = scanner.nextInt();
        double correctAnswer = (((double)a/b));
        System.out.printf("Value: %.3f\n", correctAnswer);
    }

    public static void getMax(){
        int one = Integer.MAX_VALUE;
        int two = Integer.MAX_VALUE;
        System.out.println((long)two+one);
    }

    public static void threeValues(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Type first number ");
        float a = scanner.nextFloat();
        System.out.println("Type second number ");
        Byte b = scanner.nextByte();
        System.out.println("Type the char ");
        char c = scanner.next().charAt(0);
        System.out.println("Float: " + a + " Byte: " + b + " char is: " + c);
    }

    public static void compare(){
        System.out.println("Enter the number you wish to enter: ");
        Scanner scanner = new Scanner(System.in);
        int amount = scanner.nextInt();
        if(amount==30){
            System.out.println("Your amount is equal to thirty");
        }
        else if(amount>30){
            System.out.println("Amount is bigger than 30");
        }
        else
            System.out.println("amount is lesser than 30");
    }

    public static void compareAgain(){
        System.out.println("Enter the first number you wish to enter: ");
        Scanner scanner = new Scanner(System.in);
        int amountOne = scanner.nextInt();
        System.out.println("Enter the second number you wish to enter: ");
        int amountTwo = scanner.nextInt();
        if(amountOne > 30 && amountTwo>30){
            System.out.println("Both amounts are bigger than 30");
        }
        else if(amountOne == 30 && amountTwo == 30){
            System.out.println("Both values are equal to thirty");
        }
        else System.out.println("Something else");
    }
}
