public class Rectangle {

    public static void main(String[] args) {
        christmasTree();
        rectangleRectangle();
    }

    public static void christmasTree(){

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10 - i; j++)
                System.out.print(" ");
            for (int k = 0; k < (2 * i + 1); k++)
                System.out.print("*");
            System.out.println();
        }
        System.out.println();

    }
    public static void rectangleRectangle(){

        for (int i = 1; i <= 10; i++){
            System.out.print("*");
            for(int j = 0; j <= 10; j++){
                if(i == 1 || i == 10 || j  == i){
                    System.out.print("*");
                }else
                System.out.print(" ");
            }
            System.out.println("*");
        }

    }
}
